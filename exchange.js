let courseUsaUahBuy = 28.3;
let courseUsaUahSale = 28.5;
document.getElementById("buyusa").innerHTML = courseUsaUahBuy;
document.getElementById("saleusa").innerHTML = courseUsaUahSale;

let courseEuroUahBuy = 33.4;
let courseEuroUahSale = 33.5;
document.getElementById("buyeuro").innerHTML = courseEuroUahBuy;
document.getElementById("saleeuro").innerHTML = courseEuroUahSale;

let courseRubUahBuy = 0.364;
let courseRubUahSale = 0.367;
document.getElementById("buyrub").innerHTML = courseRubUahBuy;
document.getElementById("salerub").innerHTML = courseRubUahSale;

buy.onclick = () => {
	const indexCurrency = document.getElementById('currency').options.selectedIndex;
	const curr = document.getElementById('currency').options[indexCurrency].text;

	const val = document.getElementById('input-value').value;
	countBuyResult(curr, val);
}

sale.onclick = () => {
	const indexCurrency = document.getElementById('currency').options.selectedIndex;
	const curr = document.getElementById('currency').options[indexCurrency].text;

	const val = document.getElementById('input-value').value;
	countSaleResult(curr, val);
}

countBuyResult = (curr, val) => {
	let result;

	switch(curr) {
		case"USA": 
  			result = val * courseUsaUahBuy;
  			break;
  		case "EURO":
  			result = val * courseEuroUahBuy;
  			break;
  		case "RUB":
  			result = val * courseRubUahBuy;
  			break;
	}
	document.getElementById('result').innerHTML = `Стоимость покупки: ${result} грн.`;
}

countSaleResult = (curr, val) => {
	let result;

	switch(curr) {
		case"USA": 
  			result = val * courseUsaUahSale;
  			break;
  		case "EURO":
  			result = val * courseEuroUahSale;
  			break;
  		case "RUB":
  			result = val * courseRubUahSale;
  			break;
	}

	document.getElementById('result').innerHTML = `Стоимость продажи: ${result} грн.`;
}

